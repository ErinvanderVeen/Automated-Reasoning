\documentclass{scrartcl}

\usepackage{listings}
\usepackage{amsmath}

\lstset{%
	basicstyle=\ttfamily\scriptsize,
	tabsize=2
}

\title{Automated Reasoning\\Part 1}
\author{Erin van der Veen}

\begin{document}

\maketitle

\section{Eight Trucks and Magic Building Blocks}
\subsection{Truck Data Structure}
In order to allow easy extensibility of the problem, we create a truck data structure.
While not absolutely necessary, this data structure contains both the maximum weight that a truck can carry, and the amount of each building block that they carry.
Storing the weight in this data structure was done to easily change the weight of the building blocks.

Storing these variables in a data structure as I did also allows us to store the trucks in an array.
In turn, this allows the use of existential and universal quantifiers over this array.
In another version of the smt2 file, I did in fact implement the formulas in this way.
Due to performance issues, however, this method is not employed in the final version.

Solving this performance issue can be achieved in multiple ways.
For example, by creating a new theory (as supported by z3).
This would allow us to created a bounded array over the trucks, without the implication that is currently required.
In turn, this could significantly increases the performance.
Due to the intended scope of this assignment (SAT Solvers, instead of z3) I did not implement such a theory.

Another option in implementing fast quantifiers would be using an enumeration type.
I chose not to implement it in this way, because that would make the definition much less readable, most notably by introducing dependent types.

\subsubsection{Weights}
I considered two options in implementing the weights of the blocks.

The first option is to create an \lstinline|Array Block Int| which is then indexed by a enumeration type \lstinline|Block|.
Similar to the proposal above, this would introduce dependent types.
However, given that this Array uses the enumeration type to index a simple Int, I would argue that (unlike the proposal above) the formulas would not become significantly harder to read.
This option was not chosen for the fact that it has no benefits over the arguably easier option.

The second option is to use the method that is currently used and simply hard code the weight values as a constraint on the trucks.
$$\bigwedge_{t \in T}w(t) \leq 800 * n(t) + 1100 * p(t) + 1000 * s(t) + 2500 * c(t) + 200 * d(t)$$
Where $T$ is the set of trucks, $w(t)$ is the maximum weight that truck can carry, $n(t)$ is the number of nuzzles that truck carries, $p(t)$ is the number of prittles that truck carries, etc.
In smt2 this is notated as:
\begin{lstlisting}
(assert (>= (w t0) (+ (* 800 (n t0)) (* 1100 (p t0))
(* 1000 (s t0)) (* 2500 (c t0)) (* 200 (d t0)))))
(assert (>= (w t1) (+ (* 800 (n t1)) (* 1100 (p t1))
(* 1000 (s t1)) (* 2500 (c t1)) (* 200 (d t1)))))
...
\end{lstlisting}

\subsection{The Number of Pallets}
There are several constraints on the number of pallets that a truck can carry.
First we define that the sum of all pallets must not exceed 8:
$$\bigwedge_{t \in T} 8 \geq n(t) + p(t) + s(t) + c(t) + d(t)$$
Subsequently, we define that we cannot carry a negative number of pallets in a truck.
$$\bigwedge_{t \in T, b \in B} nrB(t, b) \geq 0$$
Where $B$ is the enumeration of bricks, and $nrB(t, b)$ returns the number of pallets in a certain truck.

The next constraint that is encoded is the fact that there are only two trucks that can carry skipples.
Given that this is the only distinguishing property of a truck, we simply pick two trucks, and assert $s(t)$ of all other trucks equal to $0$.

Next, we assert that a truck can only carry a single pallet of nuzzles:
$$\bigwedge_{t \in T} n(t) \leq 1$$

The amount of bricks that must be transported is asserted next:
$$\bigwedge_{b \in B}nr(b) = \sum_{t \in T} nrB(t, b)$$
Where $nr(b)$ is the number of pallets that we need to transport of a certain brick.
Note, that we encode $nr(prittles)$ as $nrP$.
Later, we set the goal for z3 to be \lstinline|maximize nrP|, to retrieve the maximum amount of prittles that we can transport.
\begin{lstlisting}
(assert (= 4   (+ (n t0) (n t1) (n t2) (n t3) (n t4) (n t5) (n t6) (n t7))))
(assert (= nrP (+ (p t0) (p t1) (p t2) (p t3) (p t4) (p t5) (p t6) (p t7))))
(assert (= 8   (+ (s t0) (s t1) (s t2) (s t3) (s t4) (s t5) (s t6) (s t7))))
(assert (= 10  (+ (c t0) (c t1) (c t2) (c t3) (c t4) (c t5) (c t6) (c t7))))
(assert (= 20  (+ (d t0) (d t1) (d t2) (d t3) (d t4) (d t5) (d t6) (d t7))))
\end{lstlisting}

\subsection{Assignment a}
\begin{tabular}{|l|r|r|r|r|r||r|}
	\hline
	Truck & Nuzzles & Prittles & Skipples & Crottles & Dupples & Weight \\\hline
	0 & 1 & 1 & 6 & 0 & 0 & 7900\\\hline
	1 & 1 & 3 & 1 & 1 & 2 & 8000\\\hline
	2 & 0 & 1 & 1 & 2 & 4 & 7900\\\hline
	3 & 0 & 2 & 0 & 2 & 4 & 8000\\\hline
	4 & 0 & 2 & 0 & 2 & 4 & 8000\\\hline
	5 & 1 & 4 & 0 & 1 & 1 & 7900\\\hline
	6 & 1 & 6 & 0 & 0 & 1 & 7600\\\hline
	7 & 0 & 2 & 0 & 2 & 4 & 8000\\\hline\hline
	Sum: & 4 & 21 & 8 & 10 & 20 & \\\hline
\end{tabular}

\subsection{Assignment b}
For assignment b, we must encode that prittles and crottles cannot be in the same truck.
We do this in the following way:
$$\bigwedge_{t \in T}p(t) = 0 \vee c(t) = 0$$

This results in the following table:\\
\begin{tabular}{|l|r|r|r|r|r||r|}
	\hline
	Truck & Nuzzles & Prittles & Skipples & Crottles & Dupples & Weight \\\hline
	0 & 0 & 0 & 5 & 1 & 2 & 7900\\\hline
	1 & 1 & 0 & 1 & 2 & 4 & 7600\\\hline
	2 & 0 & 0 & 2 & 2 & 4 & 7800\\\hline
	3 & 1 & 0 & 0 & 2 & 5 & 6800\\\hline
	4 & 1 & 6 & 0 & 0 & 1 & 7600\\\hline
	5 & 0 & 7 & 0 & 0 & 1 & 7900\\\hline
	6 & 0 & 0 & 0 & 3 & 2 & 7900\\\hline
	7 & 1 & 6 & 0 & 0 & 1 & 7600\\\hline\hline
	Sum: & 4 & 19 & 8 & 10 & 20 & \\\hline
\end{tabular}

\section{Chip Design}
Like the first assignment we start by defining a data structure.
This time, the data structure encodes the information of a component.
In particular: $x$ and $y$ as the coordinates of the component, and $w$ and $h$ as the width and height of the component.
This data structure is used both for the ordinary components as the power-components (PCs).
The assignment does not specify if the coordinates should be Integers or Reals.
In order to allow easy change between the two, line 5 of the file is the only line that must be changed in order to switch between the two.

In the assignment above, I mentioned that I used to use arrays to store the data structures.
A remnant of this approach is seen in this file.
It can, in particular, be found in the function definitions.
Which, instead of \lstinline|:: Component -> Bool| will look something like: \lstinline|:: Int -> Bool|.
Here, the integer that is passed is used to retrieve the component from a global component array instead.

\subsection{Functions}
This smt2 file makes heavy use of functions.
In particular, I create 3 functions that are used to test the constrains, and 2 helper functions.
The functions are:
\begin{itemize}
	\item \lstinline|hasNoOverlap :: Int -> Bool| That checks for a component $i$ if there are no components that overlap with this component.
	\item \lstinline|inField :: Int -> Bool| That checks if a given component lies within the bounds of the chip.
	\item \lstinline|nextToAnyPC :: Int -> Bool| That checks if there is a PC that this component is connected to.
\end{itemize}

As mentioned above, there are also two helper functions.
\begin{itemize}
	\item \lstinline|noOverlap :: Int Int -> Bool| That checks if two components do not overlap.
	\item \lstinline|nextToPC :: Int Int -> Bool| That checks if a component is connected to the given PC.
\end{itemize}

\subsection{No Overlap}
The \lstinline|hasNoOverlap| function for a given component $c$ is defined as:
$$\bigwedge_{c' \in C} c = c' \vee x(c) + w(c) < x(c') \vee y(c) + h(c) < y(c') \dots$$
Where the $x$, $y$, $w$ and $h$ functions are defined as the x coordinate, y coordinate, width and height respectively, and $C$ is the set of all components.
In the actual implementation, the last section is defined by the \lstinline|noOverlap| helper function.
The above formula also shows how I chose to implement the representation of the component.
I chose to do this in the same way as was described for the example in the lectures.
\begin{lstlisting}
(define-fun noOverlap ((i Int) (j Int)) Bool
	(or
		(= j i)
		(<= (+ (x (select comps i)) (w (select comps i))) (x (select comps j)))
		(<= (+ (y (select comps i)) (h (select comps i))) (y (select comps j)))
		(<= (+ (y (select comps j)) (h (select comps j))) (y (select comps i)))
		(<= (+ (x (select comps j)) (w (select comps j))) (x (select comps i)))
	)
)
\end{lstlisting}

\subsection{No Component Out of Bounds}
Every component has to be within the bounds of the chip.
In essence, this means that the x and y coordinates of the components must not be negative, and that the coordinates plus the width/height is not allowed to reach beyond the chipsize.
The chipsize is a constant that is defined, and asserted to be equal to some value.
The above constraint is implemented using the inField function.
For some component $c$:
$$x(c) \geq 0 \wedge y(c) \geq 0 \wedge y(c) + w(c) \leq chipSize \wedge x(c) + h(c) \leq chipSize$$
Or, in smt2 form:
\begin{lstlisting}
(define-fun inField ((i Int)) Bool
	(and
		(>= (x (select comps i)) 0)
		(>= (y (select comps i)) 0)
		(<= (+ (x (select comps i)) (w (select comps i))) chipSize)
		(<= (+ (y (select comps i)) (h (select comps i))) chipSize)
	)
)
\end{lstlisting}

\subsection{Size and Rotation}
The assignment describes that the components have a set width and height, but that they may also be rotated 90 degrees.
I chose to implement this in such a way that the width of a component may also be its height, and vice versa.
\begin{lstlisting}
(assert (or (and (= (w c0) 4)  (= (h c0) 5))  (and (= (w c0) 5)  (= (h c0) 4))))
(assert (or (and (= (w c1) 4)  (= (h c1) 6))  (and (= (w c1) 6)  (= (h c1) 4))))
\end{lstlisting}

\subsection{Connection to Power Component}
The \lstinline|nextToAnyPC| function evaluates to true for some component $c$ if:
\begin{align*}
	&\exists_{p \in PC}\\
	&(y(c) + h(c) \geq y(p) \wedge y(c) \leq y(p) + h(p) \wedge (x(c) + w(c) = x(p) \vee x(c) = x(p) + w(p)))\\
	&\vee\\
	&(x(c) + w(c) \geq x(p) \wedge x(c) \leq x(p) + w(p) \wedge (y(c) + h(c) = y(p) \vee y(c) = y(p) + h(p)))
\end{align*}
While the formula might be a little complicated at first, it is actually relatively easy.
The second line checks if the component is connected to the left or right side of the power component.
It does this be first asserting that the component $c$ is at the correct height, and then asserting that the left or right edge must have the same x coordinate as the right and left edge of the PC respectively.
The third line does exactly the same, but for the case where the component is above or below the PC.
The smt2 version of the above might be more clear:
\begin{lstlisting}
(define-fun nextToPC ((i Int) (p Int)) Bool
	(or
		(and ; Left or Right of P
			(>= (+ (y (select comps i)) (h (select comps i))) (y (select comps p)))
			(<= (y (select comps i)) (+ (y (select comps p)) (h (select comps p))))
			(or
				(= (+ (x (select comps i)) (w (select comps i))) (x (select comps p)))
				(= (x (select comps i)) (+ (x (select comps p)) (w (select comps p))))
			)
		)
		(and ; Above or Below P
			(>= (+ (x (select comps i)) (w (select comps i))) (x (select comps p)))
			(<= (x (select comps i)) (+ (x (select comps p)) (w (select comps p))))
			(or
				(= (+ (y (select comps i)) (h (select comps i))) (y (select comps p)))
				(= (y (select comps i)) (+ (y (select comps p)) (h (select comps p))))
			)
		)
	)
)
\end{lstlisting}

\subsection{Applying the Formulas}
After these formulas are defined, there are subsequently enforced on all components:
$$\bigwedge_{c \in C} hasNoOverlap(c) \wedge inField(c) \wedge nextToAnyPC(c)$$
And the powercomponents:
$$\bigwedge_{p \in PC} hasNoOverlap(p) \wedge inField(p)$$
These assertions can be found at the bottom of the smt2 file.
\begin{lstlisting}
(assert (and (hasNoOverlap 0) (inField 0) (nextToAnyPC 0)))
(assert (and (hasNoOverlap 1) (inField 1) (nextToAnyPC 1)))
...
(assert (and (hasNoOverlap 10) (inField 10)))
(assert (and (hasNoOverlap 11) (inField 11)))
\end{lstlisting}

\subsection{Results}
When setting the goal for z3 to be maximizing the distance of the PC's, it returns that it is possible to fit all components on the chip, if the minimal distance is lower or equal to: 18.5.

\section{Scheduling Jobs}
Like the assignments described above, for this assignment I, again, decided to implement a custom data structure.
In this case I implement a Job data type that encodes the start $s$ and end $e$ times of the Job.

I first assert that 0 is the start time of the scheduling, and such that no job should start below 0:
$$\bigwedge_{j \in J} s(j) \geq 0$$
And that no Job should run after the deadline has passed:
$$\bigwedge_{j \in J} e(j) \leq deadline$$
Where J is the set of all jobs.

After that, I assert that the end time of every should should be equal to the start time plus its duration.
This is allowed, since we know that the jobs cannot be interrupted.
$$\bigwedge_{j \in J} e(j) = s(j) + d(j)$$
Where $d(j)$ defines the duration of j.
In smt2, this is implemented as such:
\begin{lstlisting}
(assert (= (e j1)  (+ (s j1)  6)))
(assert (= (e j2)  (+ (s j2)  7)))
(assert (= (e j3)  (+ (s j3)  8)))
(assert (= (e j4)  (+ (s j4)  9)))
...
\end{lstlisting}

The last part that is asserted is the constraints that are given by the assignment.
These constraints are 1-to-1 converted to a boolean formula, with the exception of the last.
For the last constraint (j5, j7 and j10 cannot run simultaneously) I created 3 separate formulas of the form:
$$s(j) > e(j') \vee s(j') > e(j)$$
Where j and j' are the pairs: (j5, j7), (j7, j10) and (j5, j10).

Below is the complete set constraints, and how they were implemented:
\begin{lstlisting}
; j3 can only start after j1 and j2 have finished
(assert (and (>= (s j3) (e j1) (>= (s j3) (e j2)))))
; j5 can only start after j3 and j4 have finished
(assert (and (>= (s j5) (e j3)) (>= (s j5) (e j4))))
; j7 can only start after 3,4,6 have finished
(assert (and (and (>= (s j7) (e j3)) (>= (s j7) (e j4))) (> (s= j7) (e j6))))
; j8 cannot start before j5
(assert (> (s j8) (s j5)))
; j9 can only start after j5 and j8 have finished
(assert (and (>= (s j9) (e j5)) (>= (s j9) (e j8))))
; j11 can only start after j10 has finished
(assert (>= (s j11) (e j10)))
; j12 can only start after j9 and j11 have finished
(assert (and (>= (s j12) (e j9)) (>= (s j12) (e j11))))
; j5 and j7 cannot run simultaneously
(assert (or (>= (s j5) (e j7)) (>= (s j7) (e j5))))
; j7 and j10 cannot run simultaneously
(assert (or (>= (s j7) (e j10)) (>= (s j10) (e j7))))
; j5 and j10 cannot run simultaneously
(assert (or (>= (s j5) (e j10)) (>= (s j10) (e j5))))
\end{lstlisting}

\subsection{Results}
When setting the goal for z3 to minimize the deadline, it returns that it can run all jobs before the 60th tick.

\section{Concurrent Processes}
\subsection{SMT2}
Again, I start by creating a data structure.
In this case I create the ``Triple'' type.
This triple consists of 3 integer values, that store the state of the program at some point in time.
The states are then stored in an array.
This is done such that it is possible to get the previous (or next) state, without hard coding them for every iteration.

I then define a transition function: \lstinline|defineIteration :: Int -> Bool|.
This transition function takes a point in time, and ensures that this state can be reached from the previous point in time.
\begin{lstlisting}
(define-fun defineIteration ((i Int)) Bool
	(or
		; p0
		(and
			(ite (< (val (select cvals (- i 1))) 20)
				(= (val (select cvals i)) (+ (val (select cvals (- i 1))) 1))
				(=
					(val (select cvals i))
					(+ (val (select cvals (- i 1))) (i0 (select cvals i)))
				)
			)
			(= (i0 (select cvals i)) (+ (i0 (select cvals (- i 1))) 1))
			(= (i1 (select cvals i)) (i1 (select cvals (- i 1))))
		)
		; p1
		(and
			(ite (< (val (select cvals (- i 1))) 20)
				(= (val (select cvals i)) (+ (val (select cvals (- i 1))) 1))
				(=
					(val (select cvals i))
					(+ (val (select cvals (- i 1))) (i1 (select cvals i)))
				)
			)
			(= (i1 (select cvals i)) (+ (i1 (select cvals (- i 1))) 1))
			(= (i0 (select cvals i)) (i0 (select cvals (- i 1))))
		)
	)
)
\end{lstlisting}
This function first distinguishes 2 possibilities.
Either p0 was run to get to this next state, or p1 was run.
It then first asserts the value of c, if it was below 20 in the previous state.
Next, it asserts the value of c if it was higher then 20.

After this function is defined, the last thing that remains is asserting that it holds true for every iteration.
$$\bigwedge_{s \in S}defineIteration(s)$$

We then set the initial state, and the goal:
\begin{lstlisting}
; Start c, i0 and i1 at 0
(assert (= c0 (new 0 0 0)))

; Both programs must only execute 20 times
(assert (= (i0 c40) 20))
(assert (= (i1 c40) 20))

; Determine if c40 can reach a value
(assert (= (val c40) 330))
\end{lstlisting}

\subsection{SMV}
The fact that I defined the smt version as already being an initial state with some transition, makes the implementation of the SMV equivalent much easier.
In fact, one could say that the SMV variant is a near identical translation of the SMT variant, with the exception of the order in which the operations are performed.
The SMT variant uses the i value of the next iteration to increase, while the SMV variant cannot do this.
In order to overcome this issue, without having to restructure the concept, I opted to simply start the initial state with the i values already being 1 and setting the goal to be 1 higher than we actually want.
\begin{lstlisting}
MODULE main
	VAR
		c: 0 .. 1000;
		i0: 1 .. 40;
		i1: 1 .. 40;

	INIT
		c = 0 & i0 = 1 & i1 = 1;

	TRANS
		c < 20 ?
			next(c) = c + 1 &
			(next(i0) = i0 + 1 & next(i1) = i1 |
			next(i0) = i0 & next(i1) = i1 + 1 )
		:
			next(c) = c + i0 & next(i0) = i0 + 1 & next(i1) = i1 |
			next(c) = c + i1 & next(i0) = i0 & next(i1) = i1 + 1

	LTLSPEC
		G!(c = 230 & i0 = 21 & i1 = 21)
\end{lstlisting}

\subsection{Results}
For $c = 230$, z3 returns that it is satisfiable and NuSMV returns a counterexample.
The same is true for 281 and 330.
Values for which z3 proves unsat, and NuSMV cannot create a counterexample include: 1, 282 and 1000.

The performance difference is immense, with z3 typically proving satisfiability in less then a second, where it takes NuSMV at least a few minutes.
Using command line arguments with NuSMV, to for example hint that it can only find a counter example at state 41, does slightly improve the performance, but does not reduce it to the seconds that z3 takes.
I find the documentation of NuSMV to be slightly lacking, so it might be possible to increase the performance even further.
\end{document}
