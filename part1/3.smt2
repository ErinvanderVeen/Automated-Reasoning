(set-option :produce-models true)

; Declare Task Datastructure
(declare-datatypes () ((Job (mk-job (s Int) (e Int)))))

; In order to find minimal solution
; We set a goal to minimize deadline
; Turns out 41 is the minimum
(declare-const deadline Int)
(minimize deadline)

; Create 12 Jobs
(declare-const j1 Job)
(declare-const j2 Job)
(declare-const j3 Job)
(declare-const j4 Job)
(declare-const j5 Job)
(declare-const j6 Job)
(declare-const j7 Job)
(declare-const j8 Job)
(declare-const j9 Job)
(declare-const j10 Job)
(declare-const j11 Job)
(declare-const j12 Job)

; Should not schedule below 0
(assert (and (>= (s j1) 0) (>= (s j2) 0) (>= (s j3) 0) (>= (s j4) 0) (>= (s j5) 0) (>= (s j6) 0) (>= (s j7) 0) (>= (s j8) 0) (>= (s j9) 0) (>= (s j10) 0) (>= (s j11) 0) (>= (s j12) 0)))

; And not after deadline
(assert (and (<= (e j1) deadline) (<= (e j2) deadline) (<= (e j3) deadline) (<= (e j4) deadline) (<= (e j5) deadline) (<= (e j6) deadline) (<= (e j7) deadline) (<= (e j8) deadline) (<= (e j9) deadline) (<= (e j10) deadline) (<= (e j11) deadline) (<= (e j12) deadline)))

; Define durations of all of the tasks
(assert (= (e j1)  (+ (s j1)  6)))
(assert (= (e j2)  (+ (s j2)  7)))
(assert (= (e j3)  (+ (s j3)  8)))
(assert (= (e j4)  (+ (s j4)  9)))
(assert (= (e j5)  (+ (s j5)  10)))
(assert (= (e j6)  (+ (s j6)  11)))
(assert (= (e j7)  (+ (s j7)  12)))
(assert (= (e j8)  (+ (s j8)  13)))
(assert (= (e j9)  (+ (s j9)  14)))
(assert (= (e j10) (+ (s j10) 15)))
(assert (= (e j11) (+ (s j11) 16)))
(assert (= (e j12) (+ (s j12) 17)))

; Define other constraints
; j3 can only start after j1 and j2 have finished
(assert (and (>= (s j3) (e j1) (>= (s j3) (e j2)))))
; j5 can only start after j3 and j4 have finished
(assert (and (>= (s j5) (e j3)) (>= (s j5) (e j4))))
; j7 can only start after 3,4,6 have finished
(assert (and (and (>= (s j7) (e j3)) (>= (s j7) (e j4))) (> (s= j7) (e j6))))
; j8 cannot start before j5
(assert (> (s j8) (s j5)))
; j9 can only start after j5 and j8 have finished
(assert (and (>= (s j9) (e j5)) (>= (s j9) (e j8))))
; j11 can only start after j10 has finished
(assert (>= (s j11) (e j10)))
; j12 can only start after j9 and j11 have finished
(assert (and (>= (s j12) (e j9)) (>= (s j12) (e j11))))
; j5 and j7 cannot run simultaneously
(assert (or (>= (s j5) (e j7)) (>= (s j7) (e j5))))
; j7 and j10 cannot run simultaneously
(assert (or (>= (s j7) (e j10)) (>= (s j10) (e j7))))
; j5 and j10 cannot run simultaneously
(assert (or (>= (s j5) (e j10)) (>= (s j10) (e j5))))
(check-sat)
(get-model)

(exit)
