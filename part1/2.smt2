(set-option :produce-models true)

; Create Record to store all information of a componenent
; NOTE: Components may be rotated 90 degrees, width could be height and viceversa.
(declare-datatypes () ((Component (mk-comp (x Real) (y Real) (w Real) (h Real)))))

; Define Boundary
(declare-const chipSize Real)
(assert (= chipSize 30))

; Powercomponents must be 18 apart
(declare-const dist Real)
;(assert (= dist 18))
(maximize dist)

; Declare all Components
; Index from 0 so we need only 2 chars
(declare-const c0 Component)
(declare-const c1 Component)
(declare-const c2 Component)
(declare-const c3 Component)
(declare-const c4 Component)
(declare-const c5 Component)
(declare-const c6 Component)
(declare-const c7 Component)
(declare-const c8 Component)
(declare-const c9 Component)
(declare-const p0 Component) ; Power 0
(declare-const p1 Component) ; Power 1

; Unifying with array to allow fast equality check.
(declare-const comps (Array Int Component))
(assert (= (store comps 0 c0) comps))
(assert (= (store comps 1 c1) comps))
(assert (= (store comps 2 c2) comps))
(assert (= (store comps 3 c3) comps))
(assert (= (store comps 4 c4) comps))
(assert (= (store comps 5 c5) comps))
(assert (= (store comps 6 c6) comps))
(assert (= (store comps 7 c7) comps))
(assert (= (store comps 8 c8) comps))
(assert (= (store comps 9 c9) comps))
(assert (= (store comps 10 p0) comps))
(assert (= (store comps 11 p1) comps))

; Defines sizes
(assert (or (and (= (w c0) 4)  (= (h c0) 5))  (and (= (w c0) 5)  (= (h c0) 4))))
(assert (or (and (= (w c1) 4)  (= (h c1) 6))  (and (= (w c1) 6)  (= (h c1) 4))))
(assert (or (and (= (w c2) 5)  (= (h c2) 20)) (and (= (w c2) 20) (= (h c2) 5))))
(assert (or (and (= (w c3) 6)  (= (h c3) 9))  (and (= (w c3) 9)  (= (h c3) 6))))
(assert (or (and (= (w c4) 6)  (= (h c4) 10)) (and (= (w c4) 10) (= (h c4) 6))))
(assert (or (and (= (w c5) 6)  (= (h c5) 11)) (and (= (w c5) 11) (= (h c5) 6))))
(assert (or (and (= (w c6) 7)  (= (h c6) 8))  (and (= (w c6) 8)  (= (h c6) 7))))
(assert (or (and (= (w c7) 7)  (= (h c7) 12)) (and (= (w c7) 12) (= (h c7) 7))))
(assert (or (and (= (w c8) 10) (= (h c8) 10)) (and (= (w c8) 10) (= (h c8) 10))))
(assert (or (and (= (w c9) 10) (= (h c9) 20)) (and (= (w c9) 20) (= (h c9) 10))))
(assert (or (and (= (w p0) 4)  (= (h p0) 3))  (and (= (w p0) 3)  (= (h p0) 4))))
(assert (or (and (= (w p1) 4)  (= (h p1) 3))  (and (= (w p1) 3)  (= (h p1) 4))))

; Given two arguments ensure that these two components do not overlap
(define-fun noOverlap ((i Int) (j Int)) Bool
	(or
		; If they are the same component, we do not care
		(= j i)
		(<= (+ (x (select comps i)) (w (select comps i))) (x (select comps j)))
		(<= (+ (y (select comps i)) (h (select comps i))) (y (select comps j)))
		(<= (+ (y (select comps j)) (h (select comps j))) (y (select comps i)))
		(<= (+ (x (select comps j)) (w (select comps j))) (x (select comps i)))
	)
)

; For a single component checks if it has no overlap with any other component
(define-fun hasNoOverlap ((i Int)) Bool
	(and
		(noOverlap i 0)
		(noOverlap i 1)
		(noOverlap i 2)
		(noOverlap i 3)
		(noOverlap i 4)
		(noOverlap i 5)
		(noOverlap i 6)
		(noOverlap i 7)
		(noOverlap i 8)
		(noOverlap i 9)
		(noOverlap i 10)
		(noOverlap i 11)
	)
)

; For a single component checks if the entire component is within the chip
(define-fun inField ((i Int)) Bool
	(and
		(>= (x (select comps i)) 0)
		(>= (y (select comps i)) 0)
		(<= (+ (x (select comps i)) (w (select comps i))) chipSize)
		(<= (+ (y (select comps i)) (h (select comps i))) chipSize)
	)
)

; Checks if component is connected to PC p
(define-fun nextToPC ((i Int) (p Int)) Bool
	(or
		(and ; Left or Right of P
			(>= (+ (y (select comps i)) (h (select comps i))) (y (select comps p)))
			(<= (y (select comps i)) (+ (y (select comps p)) (h (select comps p))))
			(or
				(= (+ (x (select comps i)) (w (select comps i))) (x (select comps p)))
				(= (x (select comps i)) (+ (x (select comps p)) (w (select comps p))))
			)
		)
		(and ; Above or Below P
			(>= (+ (x (select comps i)) (w (select comps i))) (x (select comps p)))
			(<= (x (select comps i)) (+ (x (select comps p)) (w (select comps p))))
			(or
				(= (+ (y (select comps i)) (h (select comps i))) (y (select comps p)))
				(= (y (select comps i)) (+ (y (select comps p)) (h (select comps p))))
			)
		)
	)
)

; Ensure that a component is next to any PC
(define-fun nextToAnyPC ((i Int)) Bool
	(or
		(nextToPC i 10)
		(nextToPC i 11)
	)
)

; Ensure PC's have a distance of dist
(assert (or
	(<= (+ (x p1) (* 0.5 (w p1)) dist) (+ (x p0) (* 0.5 (w p0))))
	(<= (+ (x p0) (* 0.5 (w p0)) dist) (+ (x p1) (* 0.5 (w p1))))
	(<= (+ (y p1) (* 0.5 (h p1)) dist) (+ (y p0) (* 0.5 (h p0))))
	(<= (+ (y p0) (* 0.5 (h p0)) dist) (+ (y p1) (* 0.5 (h p1))))
))

; Enforce constraints on components
(assert (and (hasNoOverlap 0) (inField 0) (nextToAnyPC 0)))
(assert (and (hasNoOverlap 1) (inField 1) (nextToAnyPC 1)))
(assert (and (hasNoOverlap 2) (inField 2) (nextToAnyPC 2)))
(assert (and (hasNoOverlap 3) (inField 3) (nextToAnyPC 3)))
(assert (and (hasNoOverlap 4) (inField 4) (nextToAnyPC 4)))
(assert (and (hasNoOverlap 5) (inField 5) (nextToAnyPC 5)))
(assert (and (hasNoOverlap 6) (inField 6) (nextToAnyPC 6)))
(assert (and (hasNoOverlap 7) (inField 7) (nextToAnyPC 7)))
(assert (and (hasNoOverlap 8) (inField 8) (nextToAnyPC 8)))
(assert (and (hasNoOverlap 9) (inField 9) (nextToAnyPC 9)))
(assert (and (hasNoOverlap 10) (inField 10)))
(assert (and (hasNoOverlap 11) (inField 11)))

(check-sat)
(get-model)

(exit)
