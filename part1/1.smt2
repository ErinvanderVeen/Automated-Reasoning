(set-option :produce-models true)

; w = (MAX) Weight, n = Nuzzles, p = Prittles, s = Skipples, c = Crottles, d = Dupples
(declare-datatypes () ((Truck (mk-trck (w Int) (n Int) (p Int) (s Int) (c Int) (d Int)))))

; Eight Trucks
(declare-const t0 Truck)
(declare-const t1 Truck)
(declare-const t2 Truck)
(declare-const t3 Truck)
(declare-const t4 Truck)
(declare-const t5 Truck)
(declare-const t6 Truck)
(declare-const t7 Truck)

; Max weight (capacity) of 8000kg
(assert (= 8000 (w t0)))
(assert (= 8000 (w t1)))
(assert (= 8000 (w t2)))
(assert (= 8000 (w t3)))
(assert (= 8000 (w t4)))
(assert (= 8000 (w t5)))
(assert (= 8000 (w t6)))
(assert (= 8000 (w t7)))

; Ensure that we do not go over the weight
(assert (>= (w t0) (+ (* 800 (n t0)) (* 1100 (p t0)) (* 1000 (s t0)) (* 2500 (c t0)) (* 200 (d t0)))))
(assert (>= (w t1) (+ (* 800 (n t1)) (* 1100 (p t1)) (* 1000 (s t1)) (* 2500 (c t1)) (* 200 (d t1)))))
(assert (>= (w t2) (+ (* 800 (n t2)) (* 1100 (p t2)) (* 1000 (s t2)) (* 2500 (c t2)) (* 200 (d t2)))))
(assert (>= (w t3) (+ (* 800 (n t3)) (* 1100 (p t3)) (* 1000 (s t3)) (* 2500 (c t3)) (* 200 (d t3)))))
(assert (>= (w t4) (+ (* 800 (n t4)) (* 1100 (p t4)) (* 1000 (s t4)) (* 2500 (c t4)) (* 200 (d t4)))))
(assert (>= (w t5) (+ (* 800 (n t5)) (* 1100 (p t5)) (* 1000 (s t5)) (* 2500 (c t5)) (* 200 (d t5)))))
(assert (>= (w t6) (+ (* 800 (n t6)) (* 1100 (p t6)) (* 1000 (s t6)) (* 2500 (c t6)) (* 200 (d t6)))))
(assert (>= (w t7) (+ (* 800 (n t7)) (* 1100 (p t7)) (* 1000 (s t7)) (* 2500 (c t7)) (* 200 (d t7)))))

; Ensure that we only load max 8 pallets
(assert (>= 8 (+ (n t0) (p t0) (s t0) (c t0) (d t0))))
(assert (>= 8 (+ (n t1) (p t1) (s t1) (c t1) (d t1))))
(assert (>= 8 (+ (n t2) (p t2) (s t2) (c t2) (d t2))))
(assert (>= 8 (+ (n t3) (p t3) (s t3) (c t3) (d t3))))
(assert (>= 8 (+ (n t4) (p t4) (s t4) (c t4) (d t4))))
(assert (>= 8 (+ (n t5) (p t5) (s t5) (c t5) (d t5))))
(assert (>= 8 (+ (n t6) (p t6) (s t6) (c t6) (d t6))))
(assert (>= 8 (+ (n t7) (p t7) (s t7) (c t7) (d t7))))

; We cannot have a negative amount of pallets
(assert (and (>= (n t0) 0) (>= (p t0) 0) (>= (s t0) 0) (>= (c t0) 0) (>= (d t0) 0)))
(assert (and (>= (n t1) 0) (>= (p t1) 0) (>= (s t1) 0) (>= (c t1) 0) (>= (d t1) 0)))
(assert (and (>= (n t2) 0) (>= (p t2) 0) (>= (s t2) 0) (>= (c t2) 0) (>= (d t2) 0)))
(assert (and (>= (n t3) 0) (>= (p t3) 0) (>= (s t3) 0) (>= (c t3) 0) (>= (d t3) 0)))
(assert (and (>= (n t4) 0) (>= (p t4) 0) (>= (s t4) 0) (>= (c t4) 0) (>= (d t4) 0)))
(assert (and (>= (n t5) 0) (>= (p t5) 0) (>= (s t5) 0) (>= (c t5) 0) (>= (d t5) 0)))
(assert (and (>= (n t6) 0) (>= (p t6) 0) (>= (s t6) 0) (>= (c t6) 0) (>= (d t6) 0)))
(assert (and (>= (n t7) 0) (>= (p t7) 0) (>= (s t7) 0) (>= (c t7) 0) (>= (d t7) 0)))

; Only t0, t1 and t2 are cooled and can therefore carry skipples
(assert (= 0 (s t3)))
(assert (= 0 (s t4)))
(assert (= 0 (s t5)))
(assert (= 0 (s t6)))
(assert (= 0 (s t7)))

; Ensure that no two pallets of nuzzles may be in the same truck
(assert (>= 1 (n t0)))
(assert (>= 1 (n t1)))
(assert (>= 1 (n t2)))
(assert (>= 1 (n t3)))
(assert (>= 1 (n t4)))
(assert (>= 1 (n t5)))
(assert (>= 1 (n t6)))
(assert (>= 1 (n t7)))

(declare-const nrP Int)

; Set amount of each brick
(assert (= 4   (+ (n t0) (n t1) (n t2) (n t3) (n t4) (n t5) (n t6) (n t7))))
(assert (= nrP (+ (p t0) (p t1) (p t2) (p t3) (p t4) (p t5) (p t6) (p t7))))
(assert (= 8   (+ (s t0) (s t1) (s t2) (s t3) (s t4) (s t5) (s t6) (s t7))))
(assert (= 10  (+ (c t0) (c t1) (c t2) (c t3) (c t4) (c t5) (c t6) (c t7))))
(assert (= 20  (+ (d t0) (d t1) (d t2) (d t3) (d t4) (d t5) (d t6) (d t7))))

; Prittles and Crottles cannot be in the same truck
(assert (or (= (p t0) 0) (= (c t0) 0)))
(assert (or (= (p t1) 0) (= (c t1) 0)))
(assert (or (= (p t2) 0) (= (c t2) 0)))
(assert (or (= (p t3) 0) (= (c t3) 0)))
(assert (or (= (p t4) 0) (= (c t4) 0)))
(assert (or (= (p t5) 0) (= (c t5) 0)))
(assert (or (= (p t6) 0) (= (c t6) 0)))
(assert (or (= (p t7) 0) (= (c t7) 0)))

(maximize nrP)

(check-sat)
(get-model)

(exit)
