#!/usr/bin/env python3

import sys
nr_states = int(sys.argv[1])


def as_string(i):
    if i < 10:
        return "0" + str(i)
    return str(i)

print("(set-option :produce-models true)")

sys.stdout.write("(declare-datatypes () ((State")
for i in range(0, nr_states):
    sys.stdout.write(" s" + str(i))
print(")))")
print("(declare-datatypes () ((Symbol A B)))")
print("(define-sort Word () (Array Int Symbol))\n")

print("; Store all final bools in an array")
print("(declare-const final (Array State Bool))")

for i in range(0, nr_states):
    f = "f" + str(i)
    s = "s" + str(i)
    print("(declare-const " + f + " Bool)")
    print("(assert (= final (store final " + s + " " + f + ")))")
print()

print("; Store all transitions in an array")
print("(declare-const transition (Array State State Symbol Bool))")
for i in range(0, nr_states):
    for j in range(0, nr_states):
        t = "t" + as_string(i) + as_string(j)
        print("(declare-const " + t + "A Bool)")
        print("(assert (= transition (store transition s" + str(i) + " s" + str(j) + " A " + t + "A)))")
        print("(declare-const " + t + "B Bool)")
        print("(assert (= transition (store transition s" + str(i) + " s" + str(j) + " B " + t + "B)))")
print()

words = ["AA", "ABA", "BAA", "ABAB", "BABB", "BAAAA", "AAABA", "ABAAA"]
words_not = ["A", "B", "AB", "BA", "BB", "AAA", "AAB", "ABB", "BAB", "BBA", "BBB", "AAAA", "AAAB", "AABA", "AABB", "ABAA", "ABBA", "ABBB", "BAAA", "BAAB", "BABA", "BBAA", "BBAB", "BBBA", "BBBB"]

word_counter = 0

def path(prev, cur, word, ltr):
    sys.stdout.write(" (and (select transition s" + str(prev) + " s" + str(cur) + " " + word[ltr] + ")")
    if ltr is len(word) - 1:
        sys.stdout.write(" (select final s" + str(cur) + "))")
        return
    sys.stdout.write(" (or ")
    for i in range(0, nr_states):
        path(cur, i, word, ltr + 1)
    sys.stdout.write("))")
    return

for word in words + words_not:
    sys.stdout.write("(assert ")
    if word in words_not:
        sys.stdout.write("(not ")
    sys.stdout.write("(or ")
    for i in range(0, nr_states):
        path(0, i, word, 0)
    if word in words_not:
        sys.stdout.write(")")
    print("))")

print("(check-sat)")
print("(get-model)")
print("(exit)")

