\documentclass{scrartcl}

\usepackage{listings}
\usepackage{amsmath}
\usepackage{minted}
\setminted{breaklines=true}
\usepackage{tikz}
\usetikzlibrary{positioning,shapes,fit,arrows,automata}

\lstset{%
	basicstyle=\ttfamily\scriptsize,
	tabsize=2
}

\title{Automated Reasoning\\Part 2}
\author{Erin van der Veen}

\begin{document}

\maketitle

\section{Four Non-Self-Supporting Villages}
\subsection{Problem Description}
There are 4 villages A,B,C,D, a truck T and an extra location S.
The goal of the truck is to deliver food to the villages in such a way that they never run out of food.
While the truck travels from one village to another the villages consume a specified amount of food.
As the truck delivers food to the villages, the food it has with it decreases.
It can restock a location S.

\subsection{Encoding}
There are obvious ways to solve this problem.

Solution one is using NuSMV and defining a transition system that encodes the traveling of the truck to each of the villages.
The only tricky thing about this encoding is the amount of food that the truck delivers at a village.
This can be solved by defining a zero-cost self loop at the villages.
When the truck follows this self loop, it delivers a single unit of food at the village.
A more efficient approach is to use random domains in NuSMV.

Solution two utilizes a SMT solver.
We create a set of variables for every time slot.
We then assert the beginning values, the transitions, and the maximum values, like we would in the SMV encoding.
$$\bigwedge_{t \in trips, v \in villages}v[t] > 0$$
$$\bigwedge_{t \in trips, v \in villages \cup \{T\}} v[t] \leq lim(v)$$
Where $lim(v)$ is the maximum amount that village/truck $v$ can store.

For the transitions, we use an extra variable that holds the amount of time it took the truck to go from the previous village to the next.
This allows us to use the variable instead  of having to recalculate many times over.
I do not know if the sat solver is actually more efficient with the method, but it does significantly reduce the size of the smt2 files.
\begin{minted}{text}
(assert (ite (and (= TL0 S) (= TL1 A)) (= TT1 15) (ite (and (= TL0 S) (= TL1 C)) (= TT1 15) (ite (and (= TL0 A) (= TL1 S)) (= TT1 15) (ite (and (= TL0 A) (= TL1 B)) (= TT1 17) (ite (and (= TL0 A) (= TL1 C)) (= TT1 11) (ite (and (= TL0 B) (= TL1 A)) (= TT1 17) (ite (and (= TL0 B) (= TL1 C)) (= TT1 11) (ite (and (= TL0 B) (= TL1 D)) (= TT1 20) (ite (and (= TL0 C) (= TL1 S)) (= TT1 15) (ite (and (= TL0 C) (= TL1 A)) (= TT1 11) (ite (and (= TL0 C) (= TL1 B)) (= TT1 11) (ite (and (= TL0 C) (= TL1 D)) (= TT1 20) (ite (and (= TL0 D) (= TL1 C)) (= TT1 20) (ite (and (= TL0 D) (= TL1 B)) (= TT1 20) false)))))))))))))))
\end{minted}

The transitions are defined using the \texttt{ite} statement from the smt2 language.
\begin{minted}{text}
(assert (= A1 (ite (and (= TL1 A) (> A0 TT1)) (- (+ A0 (- TS0 TS1)) TT1) (- A0 TT1))))
\end{minted}
In the above examples, \texttt{TLi, TTi, Ai} are the truck location, transition time and supply of village A at time i respectively.

Unfortunately, theoretically we cannot determine if it is impossible for all villages to always have a food supply.
We can, however, ask the SAT solver to determine if it is possible for the villages to have food for N trips of the truck.

The same holds true for assignment B. Generally speaking we cannot determine if something holds forever.
We can, however, ask the SMT solver to find a cycle.
I do this by specifying that for the last state, that there must exists another state for which the location of the truck is the same, and the supplies for each of the villages, and truck, must be lower or equal to the last state.
If this holds, then there must exists a cycle in the path of the truck, with which it is able to ensure that every village always has food.

\subsection{Result for A}
Z3 is unable to satisfy the formula given a truck capacity of 235 when it can make 32 trips.
If it cannot satisfy the formula for 32 trips, it must also be true that the villages cannot be supplied indefinitely.

\subsection{Result for B}
Unfortunately, it takes too long for my computer to calculate the satisfiability of this formula.

\section{Minimal NFA}
\subsection{Problem Description}
The goal of this exercise is to use some automated reasoning tool to generate the minimal NFA that accepts some Language $L$.
In our specific case that language is defined as:
\begin{minted}{Python}
A = ["AA", "ABA", "BAA", "ABAB", "BABB", "BAAAA", "AAABA", "ABAAA"]
R = ["A", "B", "AB", "BA", "BB", "AAA", "AAB", "ABB", "BAB", "BBA", "BBB", "AAAA", "AAAB", "AABA", "AABB", "ABAA", "ABBA", "ABBB", "BAAA", "BAAB", "BABA", "BBAA", "BBAB", "BBBA", "BBBB"]
\end{minted}
For any words that are not specified in either \mintinline{Python}{A} or \mintinline{Python}{R}, accepting or rejecting is undefined.
Any behavior is acceptable.

\subsection{Encoding}
We create a Python script that generates smt2 formated files given a number of states.
For each of number of states, we then check if an automata can be generated.
The minimal number for which we can create an automaton is then the minimal automaton.

We create a enumeration type State that holds the states.
Every state is numbered $s_i$ where $i$ is its number.
$s_0$ is set to be the initial state (as is common).

We create an enumeration type \texttt{Symbol} that contains the Symbols present in $\Sigma$.
This is done to generalize the problem.

Furthermore, we define a \texttt{Word} as an Array of Symbols.
Again, this generalizes the solution to also accept any other words.

A function $final: Q \mapsto Bool$ is created. Another function $accepts: Word \times Int \times Q \mapsto Bool$ is also created and defined by if a given state accepts a word starting from the Integer.
In reality, another Integer is also given as argument to denote the end of the Word.
The function is defined as:
\begin{align*}
	\forall_{w, i, s}& accepts(w, i, s) \Leftrightarrow\\
	&endofword(w) \wedge final(s) \vee\\
	&\exists_{y \in Q} transition(s, y, w[i]) \wedge accepts(w, i + 1, y)
\end{align*}
Where $transition: Q \times Q \times \Sigma \mapsto Bool$ defines if there exists a transition from the first state to the second state with the given symbol.
For the initial state $s_0$ we then assert:
$$\bigwedge_{w \in A} accepts(w, 0, s0)$$
and
$$\bigwedge_{w \in R} \neg accepts(w, 0, s0)$$

\subsection{Solution}
Z3 returns that the problem is unsatisfiable with 7 states, but satisfiable with 8 states and above. This is the automata that it generates for 8 states:
\begin{figure}[H]
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
		node distance=2.8cm,semithick]

		\node[initial,accepting,state] (0)                    {$q_0$};
		\node[state]                   (1) [below of=0]       {$q_1$};
		\node[accepting,state]         (2) [below right of=1] {$q_2$};
		\node[state]                   (3) [right of=2]       {$q_3$};
		\node[state]                   (4) [above right of=3] {$q_4$};
		\node[state]                   (5) [above of=4]       {$q_5$};
		\node[state]                   (6) [above left of=5]  {$q_6$};
		\node[state]                   (7) [left of=6]        {$q_7$};

		\path
		(0) edge                  node {A}   (5)
		    edge                  node {A}   (1)
		    edge                  node {B}   (4)
		(1) edge                  node {B}   (3)
		    edge                  node {A}   (2)
		(2) edge                  node {B}   (4)
		    edge                  node {A,B} (5)
		(3) edge                  node {A}   (6)
		    edge                  node {A}   (0)
		(4) edge                  node {A}   (7)
		(5) edge                  node {B}   (3)
		(6) edge                  node {A}   (7)
		    edge                  node {B}   (0)
		(7) edge                  node {B}   (6)
		    edge                  node {A}   (0)
		    edge                  node {A}   (2);

	\end{tikzpicture}
	\caption{Solution to Problem 2}
	\label{fig:P2}
\end{figure}

\subsection{Generalization}
Since we use functions instead of variables to define each variable in the solution, generalization to more states is trivial.
The same holds true for extending the number of Symbols.
We can do this since we define the closed enumeration type, which allows z3 to efficiently generate the formulas.

\section{Groups and Abelianess}
\subsection{Group Properties}
We create a file containing the following properties:
\begin{minted}{text}
	x * (y * z) = (x * y) * z.
	x * e = x.
	x * inv(x) = e.
\end{minted}
With these properties Prover9 is able to prove:
\begin{minted}{text}
	e * x = x.
	inv(inv(x)) = x.
	inv(x) * x = e.
\end{minted}
The proves are given as follows:
\begin{minted}{text}
PROOF 1:
1 e * x = x # label(non_clause) # label(goal).  [goal].
4 x * (y * z) = (x * y) * z.  [assumption].
5 (x * y) * z = x * (y * z).  [copy(4),flip(a)].
6 x * e = x.  [assumption].
7 x * inv(x) = e.  [assumption].
8 e * c1 != c1.  [deny(1)].
11 x * (e * y) = x * y.  [para(6(a,1),5(a,1,1)),flip(a)].
12 x * (inv(x) * y) = e * y.  [para(7(a,1),5(a,1,1)),flip(a)].
17 e * inv(inv(x)) = x.  [para(7(a,1),12(a,1,2)),rewrite([6(2)]),flip(a)].
19 x * inv(inv(y)) = x * y.  [para(17(a,1),5(a,2,2)),rewrite([6(2)])].
20 e * x = x.  [para(17(a,1),11(a,2)),rewrite([19(5),11(4)])].
21 $F.  [resolve(20,a,8,a)].

PROOF 2:
2 inv(inv(x)) = x # label(non_clause) # label(goal).  [goal].
4 x * (y * z) = (x * y) * z.  [assumption].
5 (x * y) * z = x * (y * z).  [copy(4),flip(a)].
6 x * e = x.  [assumption].
7 x * inv(x) = e.  [assumption].
9 inv(inv(c2)) != c2.  [deny(2)].
11 x * (e * y) = x * y.  [para(6(a,1),5(a,1,1)),flip(a)].
12 x * (inv(x) * y) = e * y.  [para(7(a,1),5(a,1,1)),flip(a)].
17 e * inv(inv(x)) = x.  [para(7(a,1),12(a,1,2)),rewrite([6(2)]),flip(a)].
19 x * inv(inv(y)) = x * y.  [para(17(a,1),5(a,2,2)),rewrite([6(2)])].
20 e * x = x.  [para(17(a,1),11(a,2)),rewrite([19(5),11(4)])].
24 x * (inv(x) * y) = y.  [back_rewrite(12),rewrite([20(5)])].
28 inv(inv(x)) = x.  [para(7(a,1),24(a,1,2)),rewrite([6(2)]),flip(a)].
29 $F.  [resolve(28,a,9,a)].

PROOF 3:
3 inv(x) * x = e # label(non_clause) # label(goal).  [goal].
4 x * (y * z) = (x * y) * z.  [assumption].
5 (x * y) * z = x * (y * z).  [copy(4),flip(a)].
6 x * e = x.  [assumption].
7 x * inv(x) = e.  [assumption].
10 inv(c3) * c3 != e.  [deny(3)].
11 x * (e * y) = x * y.  [para(6(a,1),5(a,1,1)),flip(a)].
12 x * (inv(x) * y) = e * y.  [para(7(a,1),5(a,1,1)),flip(a)].
17 e * inv(inv(x)) = x.  [para(7(a,1),12(a,1,2)),rewrite([6(2)]),flip(a)].
19 x * inv(inv(y)) = x * y.  [para(17(a,1),5(a,2,2)),rewrite([6(2)])].
20 e * x = x.  [para(17(a,1),11(a,2)),rewrite([19(5),11(4)])].
24 x * (inv(x) * y) = y.  [back_rewrite(12),rewrite([20(5)])].
28 inv(inv(x)) = x.  [para(7(a,1),24(a,1,2)),rewrite([6(2)]),flip(a)].
32 inv(x) * x = e.  [para(28(a,1),7(a,1,2))].
33 $F.  [resolve(32,a,10,a)].
\end{minted}

Prover9 is not able to proof that \mintinline{text}{inv(x * y) = inv(x) * inv(y).}. In fact, mace4 is able to find a counter example with domain size 6.
\begin{minted}{text}
interpretation( 6, [number=1, seconds=0], [
        function(e, [ 0 ]),
        function(c1, [ 1 ]),
        function(c2, [ 2 ]),
        function(inv(_), [ 0, 1, 2, 4, 3, 5 ]),
        function(*(_,_), [
			   0, 1, 2, 3, 4, 5,
			   1, 0, 3, 2, 5, 4,
			   2, 4, 0, 5, 1, 3,
			   3, 5, 1, 4, 0, 2,
			   4, 2, 5, 0, 3, 1,
			   5, 3, 4, 1, 2, 0 ])
]).
\end{minted}
We can verify that this counterexample holds:
\begin{align*}
	inv(c1 * c2) &= inv(1 * 2) \\
	&= inv(4)\\
	&= 3 \\
	&\neq 4 \\
	&= 1 * 2 \\
	&= inv(1) * inv(2) \\
	&= inv(c1) * inv(c2) \\
\end{align*}

\subsection{Abelian Groups}
We use the same assumptions as with the assignment above, but we change the goal of Prover9/Mace4 to be \mintinline{text}{x * y = y * x.}. Prover9 is unable to find a proof, but Mace4 does find a counterexample:
\begin{minted}{text}
interpretation( 6, [number=1, seconds=0], [
        function(e, [ 0 ]),
        function(c1, [ 1 ]),
        function(c2, [ 2 ]),
        function('(_), [ 0, 1, 2, 4, 3, 5 ]),
        function(*(_,_), [
			   0, 1, 2, 3, 4, 5,
			   1, 0, 3, 2, 5, 4,
			   2, 4, 0, 5, 1, 3,
			   3, 5, 1, 4, 0, 2,
			   4, 2, 5, 0, 3, 1,
			   5, 3, 4, 1, 2, 0 ])
]).
\end{minted}
We see that this counterexample is the same as the counterexample of the previous assignment.
Again, we can show that this counterexample is correct:
\begin{align*}
	c1 * c2 &= 1 * 2 \\
	&= 4 \\
	&\neq 3 \\
	&= 2 * 1 \\
	&= c2 * c1 \\
\end{align*}

\subsection{Abelian Groups given Powers}
\subsubsection{$x^2 = e$}
We continue from the previous assignment but also assume the following:
\begin{minted}{text}
	pow(x,s(s(0))) = e.
	pow(x,s(0)) = x.
	pow(x,s(y)) = x * pow(x,y).
\end{minted}
The way we deal with natural numbers was taken from the slides.
In particular slide 266-271.

Prover9 is able to proof that this does result in Abelian groups:
\begin{minted}{text}
1 x * y = y * x # label(non_clause) # label(goal).  [goal].
2 x * (y * z) = (x * y) * z.  [assumption].
3 (x * y) * z = x * (y * z).  [copy(2),flip(a)].
4 x * e = x.  [assumption].
5 x * inv(x) = e.  [assumption].
6 pow(x,s(s(0))) = e.  [assumption].
7 pow(x,s(0)) = x.  [assumption].
8 pow(x,s(y)) = x * pow(x,y).  [assumption].
9 c2 * c1 != c1 * c2.  [deny(1)].
10 x * pow(x,0) = x.  [back_rewrite(7),rewrite([8(3)])].
11 x * x = e.  [back_rewrite(6),rewrite([8(4),8(3),10(3)])].
13 x * (inv(x) * y) = e * y.  [para(5(a,1),3(a,1,1)),flip(a)].
18 x * (y * (x * y)) = e.  [para(11(a,1),3(a,1)),flip(a)].
26 e * inv(inv(x)) = x.  [para(5(a,1),13(a,1,2)),rewrite([4(2)]),flip(a)].
28 e * inv(x) = x.  [para(11(a,1),13(a,1,2)),rewrite([4(2)]),flip(a)].
30 inv(x) = x.  [back_rewrite(26),rewrite([28(4)])].
32 e * x = x.  [back_rewrite(28),rewrite([30(2)])].
35 x * (x * y) = y.  [back_rewrite(13),rewrite([30(1),32(4)])].
37 x * (y * x) = y.  [para(18(a,1),35(a,1,2)),rewrite([4(2)]),flip(a)].
41 x * y = y * x.  [para(37(a,1),35(a,1,2))].
42 F.  [resolve(41,a,9,a)].
\end{minted}

This proof is much larger than what is needed for the actual proof.
But Prover9 does not guarantee the smallest proof.

\subsubsection{$x^3 = e$ / $x^4 = e$}
For these assignments we change the \mintinline{text}{pow(x,s(s(0))) = e.} assumption to \mintinline{text}{pow(x,s(s(s(0)))) = e.} and \mintinline{text}{pow(x,s(s(s(s(0))))) = e.} respectively.
For both of these, Mace4 is able to provide a counterexample.
The counterexample to $x^3 = e$ is rather large (domain size 27), so I will not provide the full example.
I will, however, show that this counterexample is correct:
\begin{align*}
	c1 * c2 &= 1 * 3 \\
	&= 7 \\
	&\neq 9 \\
	&= 3 * 1 \\
	&= c2 * c1 \\
\end{align*}

For $x^4 = e$ the domain size is only 8:
\begin{minted}{text}
	interpretation( 8, [number=1, seconds=0], [
        function(e, [ 0 ]),
        function(c1, [ 1 ]),
        function(c2, [ 2 ]),
        function(inv(_), [ 0, 1, 2, 4, 3, 5, 6, 7 ]),
        function(s(_), [ 1, 2, 3, 0, 0, 0, 0, 0 ]),
        function(*(_,_), [
			   0, 1, 2, 3, 4, 5, 6, 7,
			   1, 0, 3, 2, 5, 4, 7, 6,
			   2, 4, 0, 6, 1, 7, 3, 5,
			   3, 5, 1, 7, 0, 6, 2, 4,
			   4, 2, 6, 0, 7, 1, 5, 3,
			   5, 3, 7, 1, 6, 0, 4, 2,
			   6, 7, 4, 5, 2, 3, 0, 1,
			   7, 6, 5, 4, 3, 2, 1, 0 ]),
        function(pow(_,_), [
			   0, 0, 0, 0, 0, 0, 0, 0,
			   0, 1, 0, 1, 1, 1, 1, 1,
			   0, 2, 0, 2, 2, 2, 2, 2,
			   0, 3, 7, 4, 4, 4, 4, 4,
			   0, 4, 7, 3, 3, 3, 3, 3,
			   0, 5, 0, 5, 5, 5, 5, 5,
			   0, 6, 0, 6, 6, 6, 6, 6,
			   0, 7, 0, 7, 7, 7, 7, 7 ])
]).
\end{minted}
Verifying this counterexample is done in a similar way to the one above:
\begin{align*}
	c1 * c2 &= 1 * 2 \\
	&= 4 \\
	&\neq 3 \\
	&= 2 * 1 \\
	&= c2 * c1 \\
\end{align*}

\section{Monad Laws on the Maybe Monad}
\subsection{Maybe}
Combining exception handling with  functional programming is tricky since exceptions do not typecheck.
In order to allow exceptions in a functional programming context, an Algebraic Data Type (ADT) was created:
\mintinline{Clean}{:: Maybe a = Just a | Nothing}.
If the execution of a function fails, it should return Nothing, else it should just return the result (Just result).
For example:
\begin{minted}{Clean}
divide :: Real Real -> Maybe Real
divide _ 0 = Nothing
divide x y = Just (x / y)
\end{minted}

Now assume that we have a \mintinline{Clean}{Maybe Real} and we want to apply \mintinline{Clean}{(+) 1.0} to it.
We cannot easily do this since: \mintinline{Clean}{(+) :: Real Real -> Real}.
For this we have \mintinline{Clean}{fmap}:
\begin{minted}{Clean}
	fmap :: (Real -> Real) (Maybe Real) -> (Maybe Real)
	
	fmap ((+) 1.0) (Just 2.0) => Just 3.0
	fmap ((+) 1.0) Nothing => Nothing
\end{minted}
In reality fmap is defined as: \mintinline{Clean}{fmap :: (a -> b) (f a) -> f b}.
Where a, b are some types, and f is some ADT (for example, the Maybe).
fmap is part of the \texttt{Functor} class.

The other way around also holds true, sometimes we want to apply a function that is defined for Maybe on a normal value. For this, we have the pure function: \mintinline{Clean}{pure :: a -> m a | Functor m}.
pure is part of the \texttt{Applicative} class.

In order to chain several calculations, the \texttt{Monad} class was borrowed from Mathematics.
The following example uses the Applicative and Monad classes to divide 5 by a number, and then add 1 to it.
\begin{minted}{Clean}
	myfun :: Real -> Maybe Real
	myfun x = pure x >>= divide 5 >>= add 1 >>= return
\end{minted}
Where \texttt{>>=} is the infix bind function, and \texttt{return} is the same as \texttt{pure}.

\subsection{The Laws}
Since the Functor, Applicative and Monad classes can be defined for other Types than Maybe, there are several laws that the implementations must abide to.
These laws are the mathematical laws, and it might happen that implementations in Functional Languages do not abide by these laws. Either because the person who implemented them did not bother to proof them, or because they were too restrictive.
Ideally, these laws would hold, because it is likely that the programmer assumes that they hold.
The laws are as follows:
\begin{minted}{text}
% Functor:
fmap id x = x.
fmap (f o g) = (fmap f) o (fmap g)

% Applicative
pure id <*> x = x.
pure f <*> pure x = pure (f x).

% Monad
return x >>= f = f x.
x >>= return = x.
(x >>= f) >>= g = x >>= (\x -> f x >>= g).
\end{minted}

\texttt{<*>} is a function that is also part of the Applicative class.

\subsection{Higher Order Function}
First order logic does not normally allow the use of higher order functions, since the arity of the functions are set.
Consider the following function in Clean: \mintinline{Clean}{fmap id (Just 1)}.
In a first attempt we might translate this to Prover9 syntax as such: \mintinline{text}{fmap(id,just(1))}.
Unfortunately, Prover9 will complain, stating that the arity of the id-function is incorrect.
In order to overcome this problem, we borrow the function application function from Clean.

\mintinline{Clean}{$ :: (a -> b) a -> b} is an infix function that applies its first argument on its second argument.
If we then create a function pointer for every function in our domain, we can use this pointer, and the function application symbol to simulate higher order functions.
We can even model partial application using this method.
Consider the example from earlier, we create a function pointer (\texttt{id\_prt}) for the id-function.
Subsequently, we define:
\begin{minted}{text}
	fmap(x, nothing) = nothing.
	fmap(x, just(y)) = just(x $ y).

	id_ptr $ x = id(x).
	id(x) = x.
\end{minted}
Which allows us define the example as: \mintinline{text}{fmap(id_ptr, just(1)) = 1.}.

\subsection{Lambda Expressions}
As we can see, the last of the Monad Laws requires the use of lambda expressions.
Unfortunately, I have not found an elegant solution to model lambda expressions in First order logic.
In functional programming there exists a concept called ``point-free''.
A point free expression is an expression that does not bind any variables.
Such functions are typically created using functions like \mintinline{Clean}{flip :: (a b -> c) b a -> c}.
Consider, for example, the following function:
\begin{minted}{Clean}
	reversediv :: a a -> a | / a
	reversediv x y = y / x
\end{minted}

We can prevent having to bind the x and y variables by defining the function as such:
\begin{minted}{Clean}
	reversediv :: (a a -> a) | / a
	reversediv = flip (/)
\end{minted}

I used this method to rewrite the last law of the Monad as:
\begin{minted}{text}
	% Original
	(x >>= f) >>= g = x >>= (\x -> f x >>= g)

	% Pointfree
	(x >>= f) >>= g = flip (>>=) (g o f) x
\end{minted}
Unfortunately, this approach does make assumptions about the correctness of \texttt{>>=}.

\subsection{Implementation to First Order Logic}
Now that we have translated the laws to first order logic, we must also convert the Clean implementation of the classes for Maybe to first order logic.
This is rather straightforward given the relative simplicity of the Maybe instances.
\begin{minted}{text}
	% Functor
	fmap(x, nothing) = nothing.
	fmap(x, just(y)) = just(x @ y).

	% Applicative
	pure(x) = just(x).
	% * = <*>
	nothing * x = nothing.
	just(x) * y = fmap(x, y).

	% Monad
	bind(just(x), y) = y @ x.
	bind(nothing, x) = nothing.
\end{minted}
Note that instead of \$, I use @ for function application, this is in reference to the master thesis of Dan Ros\'en\footnote{web.student.chalmers.se/~danr/thesis-danr.pdf} who looked at equational properties in Haskell.

\subsection{Proof}
Given the above, Prover9 is able to proof all of the laws.
Given that there are quite a few laws that it proofs, I have decided to show only one in this report.
In particular, I will show the \texttt{bind(pure(x), y) = y @ x.} proof.

\begin{minted}{text}
1 bind(pure(x),y) = y @ x # label(non_clause) # label(goal).  [goal].
5 pure(x) = just(x).  [assumption].
8 bind(just(x),y) = y @ x.  [assumption].
16 just_ptr @ x = just(x).  [assumption].
17 just(x) = just_ptr @ x.  [copy(16),flip(a)].
24 (bind_prt @ x) @ y = bind(x,y).  [assumption].
25 bind(x,y) = (bind_prt @ x) @ y.  [copy(24),flip(a)].
28 bind(pure(c1),c2) != c2 @ c1.  [deny(1)].
29 (bind_prt @ (just_ptr @ c1)) @ c2 != c2 @ c1.  [copy(28),rewrite([5(2),17(2),25(5)])].
31 (bind_prt @ (just_ptr @ x)) @ y = y @ x.  [back_rewrite(8),rewrite([17(1),25(3)])].
32 $F.  [resolve(31,a,29,a)].
\end{minted}

\subsection{Future Work}
It would be very nice to create a Automated Prover that is able to use Clean syntax to proof properties.
Currently, there is effort by Camil Staps to integrate G$\forall$ST with the Cloogle\footnote{cloogle.org/} parser.
Ideally, we integrate an automated prover with this parser as well.
G$\forall$ST and this prover could then run in parallel to proof these properties.

\end{document}
